package es.mikostrategy.springmvcapplication.dao;

import es.mikostrategy.springmvcapplication.model.Developer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeveloperRepository extends CrudRepository<Developer, Long> {

}
