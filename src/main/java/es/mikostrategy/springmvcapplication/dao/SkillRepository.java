package es.mikostrategy.springmvcapplication.dao;

import es.mikostrategy.springmvcapplication.model.Skill;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SkillRepository extends CrudRepository<Skill, Long> {
    List<Skill> findByLabel(String label);
}